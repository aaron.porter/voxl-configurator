#!/bin/bash
################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

source /home/root/.profile.d/modalai_sku_definitions.sh


USER=$(whoami)

RESET_ALL="\e[0m"
RED="\e[91m"
YLW="\e[33m"
GRN="\e[32m"
SET_BOLD="\e[1m"

PRINT_ERROR (){
	echo -e "${RED}[ERROR] $@${RESET_ALL}"
}

PRINT_GREEEN_LINE (){
	echo -e "${GRN}${SET_BOLD}------------------------------------------------------------------${RESET_ALL}"
}

PRINT_DEBUG () {
	if $DEBUG; then
		echo "$@"
	fi
}


## local mode variables
DEBUG=false
NON_INTERACTIVE=false

# conf file paths
VOXL_PX4_CONF_FILE="/etc/modalai/voxl-px4.conf"

# set param in $VOXL_PX4_CONF_FILE
set_px4_conf_param () {
	if [ "$#" != "2" ]; then
		echo "set_px4_conf_param expected 2 args"
		exit 1
	fi

	var=$1
	val=$2
	
	sed -i "/$var=/c $var=$val" ${VOXL_PX4_CONF_FILE}
}

## set most parameters which don't have quotes in json
set_param () {
	if [ "$#" != "3" ]; then
		echo "set_param expected 3 args"
		exit 1
	fi

	CONFIG_FILE=$1
	# remove quotes if they exist
	var=$2
	var="${var%\"}"
	var="${var#\"}"
	val=$3
	val="${val%\"}"
	val="${val#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	$val," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_param_string () {
	if [ "$#" != "3" ]; then
		echo "set_param_string expected 3 args"
		exit 1
	fi
	CONFIG_FILE=$1
	var=$2
	var="${var%\"}"
	var="${var#\"}"
	sed -E -i "/\"$var\":/c\	\"$var\":	\"$3\"," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_variable () {
	if [ "$#" != "3" ]; then
		echo "set_param_string expected 3 args"
		exit 1
	fi
	CONFIG_FILE=$1
	sed -i -E "s/$2/$3/g" ${CONFIG_FILE}
}

print_usage () {
	clear -x
	echo -e ""
	PRINT_GREEEN_LINE
	echo -e "                ${GRN}${SET_BOLD}Welcome to voxl-configure-mpa!${RESET_ALL}"
	echo -e ""
	echo -e "This tool reads the pre-configured product SKU from the disk"
	echo -e "resets all MPA services and their configuration files back to a"
	echo -e "repeatable factory configuration."
	echo -e "This is performed as the last step when flashing a VOXL compute"
	echo -e "board with a platform release."
	echo -e ""
	echo -e ""
	echo -e ""
	echo -e "${GRN}${SET_BOLD}Arguments:${RESET_ALL}"
	echo -e "-d, --debug    debug mode"
	echo -e "-h, --help     show this help text"
	echo -e "-n, --non-interactive  no questions, for scripted setup"
	echo -e ""
	PRINT_GREEEN_LINE
	exit 0
}


_enable_voxl_wait_for_fs () {

	if [ -f /etc/systemd/system/voxl-wait-for-fs.service ]; then
		echo ""
		echo "enabling voxl-wait-for-fs service"
		systemctl enable voxl-wait-for-fs
		systemctl start voxl-wait-for-fs
	else
		echo ""
		echo "skipping voxl-wait-for-fs, seems to be missing"
	fi
}



_ask_continue () {
	while true; do
		echo ""
		read -p "Continue? y/n: " input
		case $input in
			[yY]*)
				echo 'Continuing'
				break
				;;
			[nN]*)
				echo 'Ok, exiting'
				exit 0
				;;
			*)
				echo 'Invalid input' >&2
		esac
	done
}

_run_step () {

	echo -e ""
	echo -e "${GRN}executing: $cmd ${RESET_ALL}"

	executable_name=${cmd%% *}

	# check to see if cmd is a local bash function
	if ! declare -F $executable_name > /dev/null; then
    	if ! [[ $(type -P "$executable_name") ]]; then
			echo -e "${RED}${SET_BOLD}System is missing $executable_name${RESET_ALL}"
			echo -e "${RED}${SET_BOLD}Likely it did not get installed or was removed${RESET_ALL}"
			HAD_FAILURE=true
			FAILURES+=("$cmd")
			return
		fi
	fi

	## now actually run the command
	$cmd
	status=$?
	if [ $status -eq 0 ]; then
		echo -e "${GRN}Success!${RESET_ALL}"
		return
	fi

	## if we got here, it failed on the first try. Wait and a try a second time
	echo -e "${YLW}first try failed, executing a second time: $cmd ${RESET_ALL}"
	sleep 1

	$cmd
	status=$?
	if [ $status -eq 0 ]; then
		echo -e "${GRN}Success running a second time${RESET_ALL}"
		HAD_SUCCESSFUL_RETRY=true
		SUCCESSFUL_RETRIES+=("$cmd")
		return
	fi

	echo -e "${RED}${SET_BOLD}FAILED TO EXECUTE: $cmd${RESET_ALL}"
	HAD_FAILURE=true
	FAILURES+=("$cmd")

	## special extra warning for the most common thing to randomly fail
	if [[ $cmd == *"voxl-configure-px4-params"* ]]; then
		echo -e "${RED}This is likely because voxl-px4 on the SDSP failed to restart.${RESET_ALL}"
		echo -e "${RED}Please try running voxl-configure-mpa again.${RESET_ALL}"
		echo -e "${RED}If that doesn't work, power cycle and try again.${RESET_ALL}"
	fi
}



_main(){

	while (( "$#" )); do
		case "$1" in

		"-d"|"--debug")
			echo "enabling debug mode"
			DEBUG=true
			;;

		"-h"|"--help")
			print_usage
			exit 0
			;;

		"-n"|"--non-interactive")
			echo "enabling non-interactive mode"
			NON_INTERACTIVE=true
			;;

		"-r"|"--reset"|"reset"|"factory"|"factory-reset"|"--factory-reset")
			echo "reset argument no longer needed"
			;;


		"-w"|"--wizard")
			echo "wizard argument no longer needed"
			;;

		*)
			echo "Invalid arg: $1, exiting"
			exit -1
			;;

		esac
		shift
	done


	PRINT_GREEEN_LINE
	echo -e "               ${GRN}${SET_BOLD}Welcome to voxl-configure-mpa!${RESET_ALL}"
	echo -e ""


	## do a quick parse, this will migrate old filename to new in case
	if ! voxl-inspect-sku --quiet; then
		exit 1
	fi

	## read from file
	VOXL_SKU=$( cat $SKU_FILENAME )
	if ! voxl-parse-and-export-sku-variables $VOXL_SKU; then
		exit 1
	fi

	## show the user what they have
	echo "VOXL is currently thinks it is in the following hardware:"
	echo ""
	voxl-print-sku-variables
	echo ""
	echo "If this doesn't look right, quit and run voxl-configure-sku to"
	echo "set it correctly. Then run voxl-configure-mpa again."

	if ! $NON_INTERACTIVE; then
		echo ""
		_ask_continue
	fi

	############################################################################
	## now for each of the _CONF_XYZ variables, construct the appropriate args
	## start with working defaults and update as appropriate
	############################################################################
	_ARGS_EXTRINSICS="m500"
	_ARGS_CAM_NUM=""
	_ARGS_CAM_ROT=""
	_ARGS_IMU="factory_enable"
	_ARGS_PX4_IMU="factory_disable"
	_ARGS_CPU_MON="factory_enable"
	_ARGS_QVIO="" ## configure later
	_ARGS_VINS="disable"
	_ARGS_DFS="factory_disable"
	_ARGS_TAG_DETECTOR="factory_disable"
	_ARGS_TFLITE="factory_disable"
	_ARGS_VVHUB="factory_enable"
	_ARGS_MAV_SERVER="factory_enable"
	_ARGS_PORTAL="enable"
	_ARGS_LEPTON="disable"
	_ARGS_FLOW="disable"
	_ARGS_TRACKER="disable"
	_ARGS_UVC="disable"
	_ARGS_PX4="factory_enable"
	_ARGS_PX4_PARAMS=""
	_ARGS_ESC=""
	_ARGS_STREAMER="factory_enable"
	_ARGS_MODEM="disable"
	_ARGS_MAVCAM="factory_enable"
	_ARGS_RANGEFINDER="disable"
	_ARGS_EXP_NUM_PARENT=0

	############################################################################
	## go through each name and populate the other _CONF_XYZ variables with
	## appropriate defaults if they are missing. Do this BEFORE generating
	## args from the conf since some _CONF_XYZ vars may still be blank from
	## an incomplete part number or not enough arguments passed to this script
	############################################################################
	case "$VOXL_FAMILY_CODE" in

		"MDK-F0001") # Flight Deck with voxl+fc or voxl-flight
			_ARGS_EXTRINSICS="m500"
			;;

		"MDK-F0002") # VOXL Deck with NO Flight Core!
			_ARGS_EXTRINSICS="m500"
			;;

		"MRB-D0001") # M500
			_ARGS_EXTRINSICS="m500"
			;;

		"MCM-C0001") # voxlcam
			_ARGS_EXTRINSICS="seeker_v1_voxlcam"
			_ARGS_CAM_ROT="rotate_stereo"
			;;

		"MRB-D0003") # Seeker V1
			_ARGS_EXTRINSICS="seeker_v1_voxlcam"
			_ARGS_CAM_ROT="rotate_stereo"
			## turn off streamer for the version without hires
			if [ $VOXL_CAM_NUM == "7" ]; then
				_ARGS_STREAMER="disable"
				_ARGS_MAVCAM="disable"
			fi
			;;

		"MRB-D0004") # Qualcomm rb5 flight 5g development drone
			_ARGS_EXTRINSICS="rb5_flight_v1"
			_ARGS_ESC="setup_sentinel_v1"
			_ARGS_PX4="sentinel_v1"
			;;

		"MRB-D0005") # Starling
			_ARGS_EXTRINSICS="starling_v2_voxl2"
			_ARGS_ELRS="--scan"
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0005-V2"
			if [ "$VOXL_HW_VERSION" == "3" ]; then
				_ARGS_ESC="setup_starling_black_motors"
			else
				_ARGS_ESC="setup_starling_silver_motors"
			fi
			;;

		"MRB-D0006"|"MDK-F0006") # Sentinel and flight deck with voxl2
			_ARGS_EXTRINSICS="sentinel_v1"
			_ARGS_ESC="setup_sentinel_v1"
			if [ "$VOXL_HW_VERSION" == "1" ]; then
				# V1 with Holybro M8 gps and spektrum radio
				_ARGS_PX4="d0006_v1"
				_ARGS_PX4_PARAMS="MRB-D0006"
			else
				# V2 with MRo M10 gps and ELRS radio
				_ARGS_PX4="d0006_v2"
				_ARGS_PX4_PARAMS="MRB-D0006-V2"
				_ARGS_ELRS="--scan"
			fi
			;;

		"MRB-D0008") # FPV
			_ARGS_EXTRINSICS="fpv_revB"
			_ARGS_LEPTON="factory_enable_flow"
			_ARGS_QVIO="factory_enable_imu_apps_trackingL"
			_ARGS_FLOW="factory_enable_lepton"
			_ARGS_TRACKER="factory_enable_lepton"
			_ARGS_PX4="d0008"
			_ARGS_PX4_PARAMS="MRB-D0008"
			if [ "$VOXL_HW_VERSION" == "4" ]; then
				_ARGS_ESC="setup_fpv_revb_v4" # m0138
				_ARGS_PX4_PARAMS="MRB-D0008-V4"
			else
				_ARGS_ESC="setup_fpv_revb_v3" # tmotor
				_ARGS_PX4_PARAMS="MRB-D0008" # tmotor reversed motor dirs
			fi
			_ARGS_STREAMER="disable"
			_ARGS_MAVCAM="disable"
			;;

		"MRB-D0010") # starling  with stereo cam and rangefinders
			_ARGS_EXTRINSICS="starling_v2_voxl2"
			_ARGS_ESC="setup_starling_black_motors"
			_ARGS_PX4="d0010"
			_ARGS_PX4_PARAMS="MRB-D0010"
			_ARGS_RANGEFINDER="4_on_m0141"
			;;

		"MRB-D0011") # PX4 Autonomy Dev Kit
			_ARGS_EXTRINSICS="d0011_starling_px4_edition"
			_ARGS_ESC="setup_starling_black_motors"
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0011"
			_ARGS_RANGEFINDER="1_downward_on_m0141"
			;;


		"MRB-D0012") # Starling 2 Max
			_ARGS_EXTRINSICS="d0012_starling_2_max"
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0012"
			_ARGS_ESC="setup_d0012"
			_ARGS_QVIO="factory_enable_imu_apps_tracking_down"
			_ARGS_RANGEFINDER="1_downward_on_m0173"
			## TODO lepton config when -X8 is enabled only
			# _ARGS_LEPTON="m0173"
			;;


		"MRB-D0013") # D0013 Stinger
			_ARGS_EXTRINSICS="D0013"
			_ARGS_ESC="setup_d0013"
			_ARGS_PX4="d0013"
			_ARGS_PX4_PARAMS="MRB-D0013"
			;;

		"MRB-D0014") # Starling 2
			_ARGS_EXTRINSICS="d0014_starling_2"
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0014"
			_ARGS_ESC="setup_d0014_starling_2"
			_ARGS_QVIO="factory_enable_imu_apps_tracking_front"
			_ARGS_RANGEFINDER="1_downward_on_m0173"
			## TODO lepton config when -X8 is enabled only
			# _ARGS_LEPTON="m0173"
			;;

		"MRB-D0015") # D0015
			#_ARGS_EXTRINSICS="D0015"
			#_ARGS_ESC="setup_d0015"
			_ARGS_PX4="d0015"
			_ARGS_PX4_PARAMS="MRB-D0015"
			;;

		"TF-M0054") # voxl2 test fixture
			_ARGS_PX4="sentinel_v1"
			_ARGS_PX4_PARAMS="MRB-D0006"
			_ARGS_QVIO="disable"
			_ARGS_STREAMER="disable"
			_ARGS_MAVCAM="disable"
			;;

		"TF-M0104") # voxl2-mini test fixture
			_ARGS_PX4="sentinel_v1"
			_ARGS_PX4_PARAMS="MRB-D0006"
			_ARGS_QVIO="disable"
			_ARGS_STREAMER="disable"
			_ARGS_MAVCAM="disable"
			;;

		"MCCA-M0054") # voxl2 board only
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0005-V2"
			_ARGS_QVIO="disable"
			_ARGS_STREAMER="disable"
			_ARGS_MAVCAM="disable"
			;;

		"MCCA-M0104") # voxl2-mini board only
			_ARGS_PX4="starling_v2"
			_ARGS_PX4_PARAMS="MRB-D0005-V2"
			_ARGS_QVIO="disable"
			_ARGS_STREAMER="disable"
			_ARGS_MAVCAM="disable"
			;;

		*)
			echo -e "${RED}[ERROR] unknown family code: ${VOXL_FAMILY_CODE}${RESET_ALL}"
			exit 1
			;;
	esac


	# clear -x
	# echo -e "${GRN}${SET_BOLD}#############################################################${RESET_ALL}"
	# echo -e "${GRN}${SET_BOLD}Hardware Configuration:${RESET_ALL}"
	# modal_print_sku_variables


	############################################################################
	## Now set up config commands based on the config
	############################################################################

	# cam num argument is same as SKU config unless set differently by the
	# family logic above
	if [ "$_ARGS_CAM_NUM" == "" ]; then
		_ARGS_CAM_NUM=${VOXL_CAM_NUM}
	fi

	# modem handling
	if [ "$VOXL_MODEM_NUM" != "" ]; then
		case "$VOXL_MODEM_NUM" in
			"18"|"19")
				_ARGS_MODEM="doodle"
				;;
			*)
				echo "No handling specified for Modem Num: ${VOXL_MODEM_NUM}"
				;;
		esac
	fi

	# misc handling
	if [ "$VOXL_MISC_NUM" != "" ]; then
		case "$VOXL_MISC_NUM" in
			"2")
				_ARGS_UVC="boson"
				;;
			"8")
				_ARGS_LEPTON="m0173"
				;;
			*)
				echo "No handling specified for Misc Num: ${VOXL_MISC_NUM}"
				;;
		esac
	fi

	# experimental handling
	if [ "$VOXL_EXP_NUM" != "" ]; then
		echo "Experimental config specified"
		# Check if the bit for "2" is set (zero rf mission)
		if (( ($VOXL_EXP_NUM & 2) != 0 )); then
			# Add the commands for bitmask 2
			echo "Executing actions for bitmask 2 (zero rf mission)"
			_ARGS_EXP_NUM_PARENT=2
			VOXL_EXP_NUM=1
		fi
		# Check if the bit for "1" is set (vins)
		if (( ($VOXL_EXP_NUM & 1) != 0 )); then
			_ARGS_QVIO="disable"
			_ARGS_VINS="enable"
			_ARGS_CPU_MON="perf"

			case "$VOXL_FAMILY_CODE" in
				"MRB-D0008") # fpv rev b
					_ARGS_FLOW="enable"
					_ARGS_LEPTON="enable"
					_ARGS_TRACKER="enable"
					_ARGS_VINS="tracking_lr_ext"
					;;

				"MRB-D0005"|"MRB-D0006"|"MRB-D0011") # px4 dev drone
					_ARGS_TRACKER="enable"
					case "$VOXL_CAM_NUM" in
					"25") 
						_ARGS_VINS="tracking_fd_ext"
						;;
					*)
						_ARGS_VINS="tracking_45_ext"
						;;
					esac
					
					;;

				"MRB-D0012"|"MRB-D0014") # starling 1/2s
					_ARGS_TRACKER="enable"
					_ARGS_VINS="tracking_fd_ext"
					;;

				*)
					echo "Experimental: ${RED}[ERROR] unsupported family code: ${VOXL_FAMILY_CODE}${RESET_ALL}"
					;;
			esac

			if (( ($_ARGS_EXP_NUM_PARENT & 2) != 0 )); then
				VOXL_EXP_NUM=$_ARGS_EXP_NUM_PARENT
			fi

			echo "Executing actions for bitmask 1 (vins)"
		fi
		# Check if the bit for "4" is set (M0065)
		if (( ($VOXL_EXP_NUM & 4) != 0 )); then
			# Add the commands for bitmask 4
			echo "Executing actions for bitmask 4 (M0065)"
		fi
		# Check if the bit for "8" is set
		if (( ($VOXL_EXP_NUM & 8) != 0 )); then
			# Add the commands for bitmask 8
			echo "Executing actions for bitmask 8"
		fi
		# Continue with other bits as needed...
	fi

	## set qVIO config based on board revision if it hasn't been set already
	if [ "$_ARGS_QVIO" == "" ]; then
		case "$VOXL_BOARD_CODE" in
			"0"|"1") # green and blue VOXL
				_ARGS_QVIO="factory_enable_imu1"
				;;
			"2") # VOXL Flight
				_ARGS_QVIO="factory_enable_imu0"
				;;
			"3") # rb5
				_ARGS_QVIO="factory_enable_imu_px4"
				_ARGS_IMU="disable"
				_ARGS_PX4_IMU="factory_enable"
				;;
			*)
				# everything else assume voxl2 or voxl2 mini.
				# everything going forward will use "imu_apps" for VIO
				_ARGS_QVIO="factory_enable_imu_apps"
				;;
		esac
	fi


	############################################################################
	## make a list of setup steps to take
	############################################################################
	CONFIG_STEPS=()

	## do standard stuff first
	CONFIG_STEPS+=("voxl-configure-extrinsics ${_ARGS_EXTRINSICS}")
	CONFIG_STEPS+=("voxl-configure-cameras ${_ARGS_CAM_NUM} ${_ARGS_CAM_ROT}")
	CONFIG_STEPS+=("voxl-configure-cpu-monitor ${_ARGS_CPU_MON}")
	CONFIG_STEPS+=("voxl-configure-qvio ${_ARGS_QVIO}")
	CONFIG_STEPS+=("voxl-configure-dfs ${_ARGS_DFS}")
	CONFIG_STEPS+=("voxl-configure-tag-detector ${_ARGS_TAG_DETECTOR}")
	CONFIG_STEPS+=("voxl-configure-tflite ${_ARGS_TFLITE}")
	CONFIG_STEPS+=("voxl-configure-vision-hub ${_ARGS_VVHUB}")
	CONFIG_STEPS+=("voxl-configure-mavlink-server ${_ARGS_MAV_SERVER}")
	CONFIG_STEPS+=("voxl-configure-portal ${_ARGS_PORTAL}")
	CONFIG_STEPS+=("voxl-configure-lepton ${_ARGS_LEPTON}")
	CONFIG_STEPS+=("voxl-configure-uvc ${_ARGS_UVC}")
	CONFIG_STEPS+=("voxl-configure-streamer ${_ARGS_STREAMER}")
	CONFIG_STEPS+=("voxl-configure-modem ${_ARGS_MODEM}")
	CONFIG_STEPS+=("voxl-configure-mavcam ${_ARGS_MAVCAM}")
	CONFIG_STEPS+=("voxl-configure-rangefinders ${_ARGS_RANGEFINDER}") # TODO check if this breaks VOXL1
	CONFIG_STEPS+=("systemctl disable voxl-static-ip")

	## then stuff that might be missing on some platforms
	if [ -f /usr/bin/voxl-configure-px4-imu-server ]; then
		CONFIG_STEPS+=("voxl-configure-px4-imu-server ${_ARGS_PX4_IMU}")
	fi
	if [ -f /usr/bin/voxl-configure-imu ]; then
		CONFIG_STEPS+=("voxl-configure-imu ${_ARGS_IMU}")
	fi
	if [ -f /usr/bin/voxl-configure-flow-server ]; then
		CONFIG_STEPS+=("voxl-configure-flow-server ${_ARGS_FLOW}")
	fi
	if [ -f /usr/bin/voxl-configure-feature-tracker ]; then
		CONFIG_STEPS+=("voxl-configure-feature-tracker ${_ARGS_TRACKER}")
	fi
	if [ -f /usr/bin/voxl-configure-open-vins ]; then
		CONFIG_STEPS+=("voxl-configure-open-vins ${_ARGS_VINS}")
	fi

	## esc should come before PX4 since px4 would need to be disabled if running
	if [ $_ARGS_ESC ]; then

		## stop and disable voxl-px4 top stop voxl-esc from stopping and restarting it
		## voxl-px4 will be re-enabled in the next step
		CONFIG_STEPS+=("systemctl stop voxl-px4")
		CONFIG_STEPS+=("systemctl disable voxl-px4")
		CONFIG_STEPS+=("sleep 3")
		CONFIG_STEPS+=("voxl-esc ${_ARGS_ESC}")
	fi

	## set up elrs if forced or transmitter is not specified
	case "$VOXL_FAMILY_CODE" in 
		"MRB-D0005"|"MRB-D0006"|"MDK-F0006"|"MRB-D0008"|"MRB-D0010"|"MRB-D0011"|"MRB-D0012"|"MRB-D0013"|"MRB-D0014"|"MRB-D0015")
			if [ "$VOXL_TRANSMITTER_NUM" == "7" ] || [ "$VOXL_TRANSMITTER_NUM" == "8" ] || [ "$VOXL_TRANSMITTER_NUM" == "" ]; then
				CONFIG_STEPS+=("voxl-elrs --scan")
			fi
			;;
		*)
			echo "[INFO] Not looking at transmitter for this SKU"
			;;
	esac

	if [ -f /usr/bin/voxl-configure-px4 ]; then
		## remove px4 params here before starting px4 for the first time
		## deleting it later in voxl-px4-params doesn't work reliably
		## no need to stop voxl-px4 since it should have been stopped before
		## the esc step, but stop it anyway just in case
		CONFIG_STEPS+=("systemctl stop voxl-px4")
		CONFIG_STEPS+=("rm -f /data/px4/param/parameters")
		CONFIG_STEPS+=("voxl-configure-px4 ${_ARGS_PX4}")
	fi

	if [ -f /usr/bin/voxl-configure-px4-params ] && [ $_ARGS_PX4_PARAMS ]; then
		CONFIG_STEPS+=("systemctl stop voxl-mavlink-server")
		CONFIG_STEPS+=("sleep 1")
		CONFIG_STEPS+=("voxl-configure-px4-params --non-interactive --platform ${_ARGS_PX4_PARAMS}")
	fi

	############################################################################
	## Now apply all these arguments one script at a time
	############################################################################
	echo ""
	echo -e "${GRN}${SET_BOLD}About to Execute:${RESET_ALL}"
	for cmd in "${CONFIG_STEPS[@]}"; do echo -e "$cmd"; done

	## in debug mode we probably want to pause here to inspect what
	## was just printed
	if $DEBUG && ! $NON_INTERACTIVE; then
		echo ""
		_ask_continue
	fi

	## write sku to disk before we forget or crash
	mkdir -p /data/modalai/
	if [ ! -d /data/modalai/ ]; then
		echo -e $RED"ERROR in voxl-configure-mpa, failed to create"
		echo -e "/data/modalai/ directory, please run as root" $RESET_ALL
		exit 1
	fi
	echo ${VOXL_SKU} > ${SKU_FILENAME}
	if [ ! -f ${SKU_FILENAME} ]; then
		echo -e $RED"ERROR in voxl-configure-mpa, failed to create"
		echo -e "$SKU_FILENAME, please run as root" $RESET_ALL
		exit 1
	fi

	## make sure voxl-suite isn't ancient before we start
	if ! $DEBUG && [ ! -f /usr/bin/voxl-vision-hub ]; then
		echo -e $RED
		if [ -f /usr/bin/voxl-vision-px4 ]; then
			echo "voxl-vision-px4 has been replaced by voxl-vision-hub"
			echo "please upgrade voxl-suite"
			echo "on VOXL1 you may need to remove it manually:"
			echo "opkg remove voxl-vision-px4 --force-depends"
		else
			echo "missing voxl-vision-hub, are you running this on a VOXL?"
		fi
		echo -e $RESET_ALL
		exit 1
	fi

	## always make sure wait-for-fs is enabled as this will set up file system
	## sync kernel parameters
	_enable_voxl_wait_for_fs


	# px4 transmitter handling, needs to be done after px4 has been configured
	if [ "$VOXL_TRANSMITTER_NUM" != "" ]; then
		case "$VOXL_TRANSMITTER_NUM" in
			"1") # spektrum
				CONFIG_STEPS+=("set_px4_conf_param RC SPEKTRUM")
				;;
			"6"|"7"|"8") # tbs crossfire or elrs
				CONFIG_STEPS+=("set_px4_conf_param RC CRSF_RAW")
				;;
			"9") # ghost rc
				CONFIG_STEPS+=("set_px4_conf_param RC GHST")
				;;
			*)
				echo "No handling specified for Transmitter Num: ${VOXL_TRANSMITTER_NUM}"
				;;
		esac
	fi

	# voxl-streamer customizations for certain camera configs
	if [ "$VOXL_CAM_NUM" != "" ]; then
		case "$VOXL_CAM_NUM" in
			"28"|"29"|"30") # hires_front
			# set voxl-streamer to hires_front_small_encoded
				CONFIG_STEPS+=("set_param_string /etc/modalai/voxl-streamer.conf input-pipe hires_front_small_encoded")
				;;
			*)
				echo "No custom voxl-streamer mods for: ${VOXL_CAM_NUM}"
				;;
		esac
	fi

	# experimental handling
	if [ "$VOXL_EXP_NUM" != "" ]; then
		echo "Experimental config specified"
		#defaults
		CONFIG_STEPS+=("set_param /etc/modalai/voxl-mavlink-server.conf autopilot_mission_delay_start -1")
		# Check if the bit for "2" is set (zero rf mission)
		if (( ($VOXL_EXP_NUM & 2) != 0 )); then
			# Add the commands for bitmask 2
			echo "Executing actions for bitmask 2"
			CONFIG_STEPS+=("voxl-configure-px4-params --non-interactive --file /usr/share/modalai/px4_params/v1.14/EKF2_helpers/indoor_vio_missing_gps.params")
			CONFIG_STEPS+=("set_param /etc/modalai/voxl-mavlink-server.conf autopilot_mission_delay_start 45")
			CONFIG_STEPS+=("voxl-configure-px4-params --non-interactive --file /usr/share/modalai/px4_params/v1.14/experimental_do_not_use/zero_rf_missions.params")
			CONFIG_STEPS+=("set_variable /usr/bin/voxl-static-ip LOCAL_IP=\"\" LOCAL_IP="\"192.168.8.20\")
			CONFIG_STEPS+=("set_variable /usr/bin/voxl-static-ip wlan0 eth0")
			CONFIG_STEPS+=("systemctl enable voxl-static-ip")
			_ARGS_EXP_NUM_PARENT=2
			VOXL_EXP_NUM=1
		fi
		# Check if the bit for "1" is set (vins)
		if (( ($VOXL_EXP_NUM & 1) != 0 )); then
			echo "Executing actions for bitmask 1"
			CONFIG_STEPS+=("set_px4_conf_param GPS NONE")

			case "$VOXL_CAM_NUM" in
				"12") # fpv rev b
					CONFIG_STEPS+=("set_param_string /etc/modalai/voxl-camera-server.conf name tracking")
					;;

				*)
					echo "Experimental: no conf tweaks needed"
					;;
			esac

			CONFIG_STEPS+=("voxl-configure-px4-params --non-interactive --file /usr/share/modalai/px4_params/v1.14/experimental_do_not_use/vins_vio_outdoor.params")

			if (( ($_ARGS_EXP_NUM_PARENT & 2) != 0 )); then
				VOXL_EXP_NUM=$_ARGS_EXP_NUM_PARENT
			fi
		fi
		# Check if the bit for "4" is set (M0065)
		if (( ($VOXL_EXP_NUM & 4) != 0 )); then
			# Add the commands for bitmask 4
			echo "Executing actions for bitmask 4"
			CONFIG_STEPS+=("set_px4_conf_param ESC VOXL2_IO_PWM_ESC")
			CONFIG_STEPS+=("set_px4_conf_param RC M0065_SBUS")
			CONFIG_STEPS+=("voxl-configure-px4-params --non-interactive --file /usr/share/modalai/px4_params/v1.14/experimental_do_not_use/voxl2_io.params")
		fi
		# Check if the bit for "8" is set
		if (( ($VOXL_EXP_NUM & 8) != 0 )); then
			# Add the commands for bitmask 8
			echo "Executing actions for bitmask 8"
		fi
		# Continue with other bits as needed...
	fi

	############################################################################
	## execute everything in the list
	############################################################################
	HAD_FAILURE=false
	FAILURES=()
	HAD_SUCCESSFUL_RETRY=false
	SUCCESSFUL_RETRIES=()

	set +e
	for cmd in "${CONFIG_STEPS[@]}"; do
		_run_step "${cmd}"
	done

	## very important, sync everything to disk before saying we are done!!
	sync

	## check if any extra calibration is needed so we can warn the user
	echo -e ""
	voxl-check-calibration --quiet
	MISSING_CAL=$?
	COLOR=${GRN}

	if $HAD_SUCCESSFUL_RETRY; then
		echo -e ""
		PRINT_GREEEN_LINE
		echo -e "${YLW}${SET_BOLD}         The following steps worked on the second try:${RESET_ALL}"
		for cmd in "${SUCCESSFUL_RETRIES[@]}"; do
			echo -e "${YLW}$cmd${RESET_ALL}"
		done
	fi

	if ! $HAD_FAILURE; then
		echo -e ""
		PRINT_GREEEN_LINE
		echo -e "${GRN}${SET_BOLD}        SUCCESSFULLY CONFIGURED MPA SERVICES!${RESET_ALL}"
		echo -e "${GRN}${SET_BOLD}        Services will start up on next reboot${RESET_ALL}"
		echo -e ""
	else
		echo -e ""
		PRINT_GREEEN_LINE
		echo -e "${RED}${SET_BOLD}         FAILED TO EXECUTE the following steps:${RESET_ALL}"
		for cmd in "${FAILURES[@]}"; do
			echo -e "${RED}$cmd${RESET_ALL}"
		done
		echo -e ""
		echo -e "${RED}${SET_BOLD}      Encountered Problems Configuring MPA Services :-/${RESET_ALL}"
		echo -e "${RED}${SET_BOLD}        Some Services may not start up on next reboot${RESET_ALL}"
		echo -e ""
		COLOR=${RED}
	fi


	#rerun cal file check, this time printing the result
	voxl-check-calibration

	echo -e ""
	echo -e "${GRN}${SET_BOLD}                PLEASE POWER CYCLE YOUR VOXL${RESET_ALL}"
	PRINT_GREEEN_LINE
	echo -e ""

	if $HAD_FAILURE; then
		exit 1
	fi

	exit 0
}



_main "$@"

