0.6.6
    *added a -j/--json option to voxl-check-calibration
0.6.5
    * force no RF assistance in flight (px4)
0.6.4
    * support hot swap between E1 and E2
0.6.3
    * update E4 config
0.6.2
    * D0012/D0014 fixes
    * update transmitter list and logic
0.6.1
    * add -X option for lepton
0.6.0
    * add voxl-streamer config customization based on camera config
0.5.9
    * only scan ELRS when necessary
0.5.8
    * start D0012 and D0014
0.5.7
    * enable M0065 experimental mode
    * start D0015 implementation
0.5.6
    * apq bug fix
0.5.5
    * updated D0008-V4 support
0.5.4
    * extend transmitter handling
    * add ghost rc support
0.5.3
    * extend error handling
0.5.2
    * add experimental mode handling
    * add voxl-configure-open-vins framework
    * kill voxl-mavlink-server before setting px4 params
    * inital support for D0012
0.5.1
    * add support for fpv revb v4
    * remove px4 params file before seting up voxl-px4 to ensure no old params survive
0.5.0
    * fix D0013 board type
0.4.9
    * new cal file check for dual ar0144 cams
0.4.8
    * add ELRS scan and update to sentinel V2 config
0.4.7
    * small polishes to voxl-configure-sku
    * add new V3 version to starling to reflect new black motors
    * D0010 and D0011 now use ESC params for black motors
0.4.6
    * set px4 params for D0006 sentinel V2
0.4.5
    * D0011 enable d0011 extrinsics
    * enable new downward mode for rangefinder on D0011
0.4.4
    * D0010 moved to new D0010 px4 params
0.4.3
    * add voxl2-mini support in qvio section of configure-mpa
0.4.2
    * fix D0013 config
0.4.1
    * treat sentinel and voxl2 flight deck the same
0.4.0
    * add sku handling for -M, -T, and -X
0.3.9
    * new config options for configuring px4 for d0006-v2, d0010, and d0013
    * differentiate Sentinel D0006 V1 and V2
    * voxl-configure-mpa adds a more helpful error message if a package is missing
    * better seeker support for both cam config options
0.3.8
    * set up d0010 calibration list
0.3.7
    * fix rb5 imu disable flag
0.3.6
    * add retry feature to voxl-configure-mpa
0.3.5
    * finish D0010 and D0011 integration
    * voxl-configure-sku polished cam config step allows to auto-choose default
0.3.4
    * first pass at adding D0010 and D0011
0.3.3
    * fix board-only custom camera sku parsing
0.3.2
    * add support for custom camera config to construct sku wizard
0.3.1
    * add support for -CC flag in SKU for custom camera config
0.3.0
    * add cal files for starling with stereo
    * add lepton lens cal for fpv
    * fix fpv lens cal filename
    * remove redundant line in voxl-configure-mpa
0.2.9
    * Added D0004 factory test
    * Added D0004 json
    * Add px4-imu test
0.2.8
    * Fix voxl-camera-server pipe names in voxl-health-check 
    * Fix small input_rc bug in voxl-health-check 
0.2.7
    * fix bug in voxl-check-calibration
    * fix bug in configure mpa when enabling mavcam manager
    * ending print for voxl-configure-sku is more accurate
    * add helpful instructions to voxl-configure-mpa when configure-px4-params fails
0.2.6
    * support new config file in voxl-mavcam-manager
0.2.5
    * add configure voxl-elrs to starling V2 configure process
0.2.4
    * fix voxl-configure-mpa non-interactive mode not printing final status report on error
0.2.3
    * fix missing /data/modalai
0.2.2
    * fix name of accelerometer px4 cal file
0.2.1
    * add px4 backup cal param files to list
0.2.0
    * fix MCCA- custom options
0.1.9
    * fix M0104 board code + add restart option to wizard
0.1.8
    * add bare board options for voxl2 and voxl-2-mini
0.1.7
    * small test fixture fixes
0.1.6
    * add various test fixtures
0.1.5
    * add default cam config suggestion to wizard
    * allow restaring the wizard instead of just quitting
0.1.4
    * fix voxl2-mini sku code number
0.1.3
    * remove references to factory_mode in voxl-health-check and replace with sku
    * update "quit" to "accept and continue" option in voxl-configure-sku
0.1.2
    * auto-detect board in wizard where possible
0.1.1
    * Add voxl-health-check
    * Update dependencies
    * Setup nightly CI job for D0008
    * voxl-health-check prints human-readable format by default now
    * add px4-param args for sentinel
    * split voxl-configure-sku out of voxl-configure-mpa
    * new tool voxl-inspect-sku
    * huge configure-mpa cleanup
0.0.4
    * fpv cam and vio config tweaks
    * improve printout at end
    * improve check calibration print
0.0.3
    * update closeout text
0.0.2
    * add voxl-configure-mavcam to the configure steps (enabled)
0.0.1
    * first release, just add voxl-configure-mpa and voxl-check-calibration
