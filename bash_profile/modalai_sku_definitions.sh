#!/bin/bash
################################################################################
# Copyright 2024 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################


export SKU_FILENAME="/data/modalai/sku.txt"

## OLD DEPRECATED DO NOT USE
export FACTORY_FILENAME="/data/modalai/factory_mode.txt"

# WARNING: DO NOT UPDATE THIS LIST WITHOUT ALSO UPDATING $VOXL_FAMILY_NAMES in the same order
export VOXL_FAMILY_CODES=(
	"MRB-D0005"
	"MRB-D0006"
	"MRB-D0008"
	"MRB-D0010"
	"MRB-D0011"
	"MRB-D0012"
	"MRB-D0013"
	"MRB-D0014"
	"MRB-D0015"
	"MRB-D0001"
	"MCM-C0001"
	"MRB-D0003"
	"MRB-D0004"
	"MDK-F0001"
	"MDK-F0002"
	"MDK-F0006"
	"TF-M0054"
	"TF-M0104"
	"MCCA-M0054"
	"MCCA-M0104")

# WARNING: DO NOT UPDATE THIS LIST WITHOUT ALSO UPDATING $VOXL_FAMILY_CODES in the same order
export VOXL_FAMILY_NAMES=(
	"starling"
	"sentinel"
	"fpv"
	"D0010"
	"px4-autonomy-dev-kit"
	"starling-2-max"
	"stinger"
	"starling-2"
	"D0015"
	"m500"
	"voxlcam"
	"seeker"
	"rb5-flight"
	"flight-deck"
	"voxl-deck"
	"voxl2-flight-deck"
	"voxl2-test-fixture"
	"voxl2-mini-test-fixture"
	"voxl2-board-only"
	"voxl2-mini-board-only")


export NUM_VOXL_FAMILIES="${#VOXL_FAMILY_CODES[@]}"

## names must be lowercase!!!!
export VOXL_BOARD_NAMES=(
	"voxl1"
	"voxl-flight"
	"rb5"
	"voxl2"
	"flight-core-v2"
	"voxl2-mini")

export NUM_VOXL_BOARDS="${#VOXL_BOARD_NAMES[@]}"




voxl-print-family-table (){

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		echo "${VOXL_FAMILY_CODES[$i-1]} ${VOXL_FAMILY_NAMES[$i-1]}"
	done
}

voxl-print-board-table (){

	for i in `seq 1 $NUM_VOXL_BOARDS`; do
		echo "$i - ${VOXL_BOARD_NAMES[$i-1]}"
	done
}


voxl-family-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-family-code-to-name, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		if [[ "${VOXL_FAMILY_CODES[$i-1]}" = "${1}" ]]; then
			echo "${VOXL_FAMILY_NAMES[$i-1]}"
			return
		fi
	done

	echo "ERROR in voxl-family-code-to-name, unknown family code ${1}"
	echo "valid codes: ${VOXL_FAMILY_CODES[@]}"
}

voxl-family-name-to-code (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-family-name-to-code, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_FAMILIES`; do
		if [[ "${VOXL_FAMILY_NAMES[$i-1]}" = "${1}" ]]; then
			echo "${VOXL_FAMILY_CODES[$i-1]}"
			return
		fi
	done

	echo "ERROR in voxl-family-name-to-code, unknown family name ${1}"
	echo "valid codes: ${VOXL_FAMILY_CODES[@]}"
}

voxl-is-valid-family-code (){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-family-code, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_FAMILY_CODES[@]}; do
			[[ "$i" = "$1" ]] && return 0
	done
	return 1
}

voxl-is-valid-family-name(){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-family_name, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_FAMILY_NAMES[@]}; do
		[[ "$i" == "$1" ]] && return 0
	done
	return 1
}

voxl-board-code-to-name (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-board-code-to-name, expected 1 argument"
		return
	fi
	if [ "$1" -lt 1 ] || [ "$1" -gt ${NUM_VOXL_BOARDS} ]; then
		>&2 echo "ERROR in voxl-board-code-to-name, board must be between 1 and $NUM_VOXL_BOARDS"
		return
	fi
	echo "${VOXL_BOARD_NAMES[$1-1]}"
}

voxl-board-name-to-code (){

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-board-name-to-code, expected 1 argument"
		return
	fi

	for i in `seq 1 $NUM_VOXL_BOARDS`; do
		if [[ "${VOXL_BOARD_NAMES[$i-1]}" = "${1}" ]]; then
			echo $i
			return
		fi
	done

	echo "ERROR in voxl-board-name-to-code, unknown board name ${1}"
	echo "valid board names: ${VOXL_BOARD_NAMES[@]}"
}


voxl-is-valid-board-code (){
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-board-code, expected 1 argument"
		return 1
	fi
	## make sure it's a digit
	if ! [[ "$1" =~ ^[[:digit:]]+$ ]]; then
		return 1
	fi
	## make sure that digit is in range
	if [ "$1" -lt 1 ] || [ "$1" -gt ${NUM_VOXL_BOARDS} ]; then
		return 1
	fi
	return 0
}

voxl-is-valid-board-name () {
	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in voxl-is-valid-board-name, expected 1 argument"
		return 1
	fi
	for i in ${VOXL_BOARD_NAMES[@]}; do
		[[ "$i" == "$1" ]] && return 0
	done
	return 1
}

voxl-parse-and-export-sku-variables () {

	if [ "$#" -ne 1 ]; then
		>&2 echo "ERROR in parse-and-export-sku-variables, expected 1 argument"
		return 1
	fi


	## actually parse the SKU
	FRONT=$( echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+-[0-9]+" )
	VOXL_FAMILY_CODE=$(  echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+" )
	VOXL_BOARD_CODE=$(   echo "$1" | grep -Eo "^[A-Z]+-[A-Z][0-9]+-[0-9]+" | grep -Eo "[0-9]+$" )
	
	## ERROR checking
	if ! voxl-is-valid-family-code $VOXL_FAMILY_CODE ; then
		echo "ERROR bad family: $VOXL_FAMILY_CODE"
		return 1
	fi

	if [[ "$VOXL_FAMILY_CODE" != *"MCCA"* ]]; then

		VOXL_HW_VERSION=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Vv][0-9]+" | grep -Eo "[0-9]+" )

		## parse modem number
		VOXL_MODEM_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Mm][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_MODEM_NUM" == "" ]; then
			VOXL_MODEM_NUM=""
		fi

		## parse transmitter number
		VOXL_TRANSMITTER_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Tt][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_TRANSMITTER_NUM" == "" ]; then
			VOXL_TRANSMITTER_NUM=""
		fi

		## parse misc number
		VOXL_MISC_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Xx][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_MISC_NUM" == "" ]; then
			VOXL_MISC_NUM=""
		fi

		## parse experimental number
		VOXL_EXP_NUM=$(   echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Ee][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_EXP_NUM" == "" ]; then
			VOXL_EXP_NUM=""
		fi

		## parse cam number including the custom mode
		VOXL_CAM_NUM=$(      echo "$1" | sed "s/$FRONT//g" | grep -Eo "[Cc][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_CAM_NUM" == "" ]; then
			if [[ "$1" == *"-CC"* ]]; then
				VOXL_CAM_NUM="C"
			fi
		fi

		## check if we got a new cam number
		if [ "$VOXL_CAM_NUM" == "" ]; then
			echo "ERROR empty cam number"
			return 1
		fi

		if ! voxl-is-valid-board-code $VOXL_BOARD_CODE ; then
			echo "ERROR bad board code: $VOXL_BOARD_CODE"
			return 1
		fi

		if [ "$VOXL_HW_VERSION" == "" ]; then
			echo "ERROR empty version number"
			return 1
		fi

		

	else
		VOXL_CAM_NUM=$(      echo "$1" | grep -Eo "[Cc][0-9]+" | grep -Eo "[0-9]+" )
		if [ "$VOXL_CAM_NUM" == "" ]; then
			if [[ "$1" == *"-CC"* ]]; then
				VOXL_CAM_NUM="C"
			else
				VOXL_CAM_NUM="0"
			fi
		fi

		VOXL_HW_VERSION="1"
		if [[ "$VOXL_FAMILY_CODE" == "MCCA-M0054" ]]; then
			VOXL_BOARD_CODE="4"
		elif [[ "$VOXL_FAMILY_CODE" == "MCCA-M0104" ]]; then
			VOXL_BOARD_CODE="6"
		else
			echo -e "${RED}[ERROR] in voxl-parse-and-export-sku-variables, unknown family $VOXL_FAMILY_CODE"
			return 1
		fi
	fi
	
	## also create names for export
	VOXL_BOARD_NAME=$(voxl-board-code-to-name $VOXL_BOARD_CODE)
	VOXL_FAMILY_NAME=$(voxl-family-code-to-name $VOXL_FAMILY_CODE)

	## export variables for use
	export VOXL_SKU
	export VOXL_FAMILY_CODE
	export VOXL_BOARD_CODE
	export VOXL_CAM_NUM
	export VOXL_MODEM_NUM
	export VOXL_TRANSMITTER_NUM
	export VOXL_MISC_NUM
	export VOXL_EXP_NUM
	export VOXL_HW_VERSION
	export VOXL_FAMILY_NAME
	export VOXL_BOARD_NAME
}


voxl-print-sku-variables (){
	echo -e "family code:   $VOXL_FAMILY_CODE ($VOXL_FAMILY_NAME)"
	echo -e "compute board: $VOXL_BOARD_CODE ($VOXL_BOARD_NAME)"
	echo -e "hw version:    $VOXL_HW_VERSION"
	echo -e "cam config:    $VOXL_CAM_NUM"
	if [ "$VOXL_MODEM_NUM" != "" ]; then
		echo -e "modem config:  $VOXL_MODEM_NUM"
	fi

	if [ "$VOXL_TRANSMITTER_NUM" != "" ]; then
		echo -e "tx config:     $VOXL_TRANSMITTER_NUM"
	fi

	if [ "$VOXL_MISC_NUM" != "" ]; then
		echo -e "misc config:  $VOXL_MISC_NUM"
	fi

	if [ "$VOXL_EXP_NUM" != "" ]; then
		echo -e "experimental config:  $VOXL_EXP_NUM"
	fi
	
	echo -e "SKU:           $VOXL_SKU"
}


## testing functions

# voxl-print-family-table
# voxl-print-board-table
# voxl-family-code-to-name MRB-D0006
# voxl-family-name-to-code starling
# voxl-board-code-to-name 4
# voxl-board-name-to-code voxl2

# if voxl-is-valid-family-code MRB-D0006 ; then
# 	echo "VALID"
# else
# 	echo "INVALID"
# fi

# if voxl-is-valid-family-name starling ; then
# 	echo "VALID"
# else
# 	echo "INVALID"
# fi

# if voxl-is-valid-board-code 1 ; then
# 	echo "VALID"
# else
# 	echo "INVALID"
# fi

# if voxl-is-valid-board-namevoxl2-mini ; then
# 	echo "VALID"
# else
# 	echo "INVALID"
# fi

